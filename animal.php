<?php

class Animal {
    public $name;
    private $legs = 2;
    public $cold_blooded = "false";

    public function __construct ($name){
        $this->name = $name;
    }

    public function getName($name){
       
        return $this->name;
    }
    public function getLegs(){
       
        return $this->legs;
    }
    public function getCold(){
       
        return $this->cold_blooded;
    }
    


}
