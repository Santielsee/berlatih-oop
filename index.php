<?php


    require("animal.php");
    require("ape.php");
    require("frog.php");

    $sheep = new Animal("Shaun");

    echo $sheep->getName("Shaun") . "</br>" ;
    echo $sheep->getLegs() . "</br>";
    echo $sheep->getCold() . "</br></br>";

    $sungokong = new Ape("Kera Sakti");
    echo $sungokong->getName("Kera Sakti </br>");
    echo "</br>" . $sungokong->yell();
    echo "</br>";
    echo "</br>";

    $kodok = new Frog("Buduk");
    echo $kodok->getName("Buduk </br>");
    echo "</br>" . $kodok->jump();
 
?>